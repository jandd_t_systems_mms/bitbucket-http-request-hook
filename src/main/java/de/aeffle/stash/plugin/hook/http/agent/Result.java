package de.aeffle.stash.plugin.hook.http.agent;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Result {
    private int statusCode = -1;
    private String message = "N/A";
}
