package de.aeffle.stash.plugin.hook.persistence;



import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import de.aeffle.stash.plugin.hook.persistence.ao.AuditLog;
import net.java.ao.DBParam;
import net.java.ao.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class AuditLogServiceImpl implements AuditLogService {
    private final ActiveObjects activeObjects;

    @Autowired
    public AuditLogServiceImpl(@ComponentImport ActiveObjects activeObjects) {
        this.activeObjects = activeObjects;
    }

    @Override
    public AuditLog[] getAuditLog() {
        return activeObjects.find(AuditLog.class);
    }

    @Override
    public AuditLog[] getAuditLog(Repository repo) {
        return activeObjects.find(AuditLog.class, Query.select().where("REPO_ID = ?", repo.getId()));
    }

    @Override
    public AuditLog createAuditLogEntry(Repository repo, String url, Integer responseCode, String responseMessage) {
        DBParam[] dbParams = { new DBParam("REQUEST_DATE", new Date()),
                               new DBParam("REPO_ID", repo.getId()),
                               new DBParam("URL", url),
                               new DBParam("RESPONSE_CODE", responseCode),
                               new DBParam("RESPONSE_MESSAGE", responseMessage)};
        AuditLog ao = activeObjects.create(AuditLog.class, dbParams);
        return ao;
    }
}
