/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.aeffle.stash.plugin.hook.http.location;

import com.atlassian.bitbucket.setting.Settings;

import java.util.ArrayList;
import java.util.List;

public class SettingsHttpLocationReader {
	private final Settings settings;

	public SettingsHttpLocationReader(Settings settings) {
        this.settings = settings;
    }

    public List<HttpLocation> getAllFromContext() {
		ArrayList<HttpLocation> httpGetLocations = new ArrayList<HttpLocation>();

		for (int id = 1; id <= getNumberOfHttpLocations(); id++) {
			httpGetLocations.add(getUntranslatedHttpLocation(id));
		}

		return httpGetLocations;
	}

	private HttpLocation getUntranslatedHttpLocation(int id) {
		String urlString = ( id > 1 ? "url" + id : "url" );
		String skipSslString = ( id > 1 ? "skipSsl" + id : "skipSsl" );
	    String httpMethodString = ( id > 1 ? "httpMethod" + id : "httpMethod" );
        String postDataString = ( id > 1 ? "postData" + id : "postData" );
        String postContentTypeString = ( id > 1 ? "postContentType" + id : "postContentType" );

		String useAuthString = ( id > 1 ? "use_auth" + id : "use_auth" );
		if (getVersionNumber() > 1) {
			useAuthString = ( id > 1 ? "useAuth" + id : "useAuth" );
		}
		
		String userString = ( id > 1 ? "user" + id : "user" );
		String passString = ( id > 1 ? "pass" + id : "pass" );

        String branchFilterString = ( id > 1 ? "branchFilter" + id : "branchFilter" );
        String tagFilterString = ( id > 1 ? "tagFilter" + id : "tagFilter" );
        String userFilterString = ( id > 1 ? "userFilter" + id : "userFilter" );

        HttpLocation httpLocation = HttpLocation.builder()
                                                .urlTemplate(getConfigString(urlString))
                                                .sslValidationDisabled(getConfigBoolean(skipSslString))
                                                .httpMethod(getConfigString(httpMethodString))
                                                .postContentType(getConfigString(postContentTypeString))
                                                .postDataTemplate(getConfigString(postDataString))
                                                .authEnabled(getConfigBoolean(useAuthString))
                                                .user(getConfigString(userString))
                                                .pass(getConfigString(passString))
                                                .branchFilter(getConfigString(branchFilterString))
                                                .tagFilter(getConfigString(tagFilterString))
                                                .userFilter(getConfigString(userFilterString))
                                                .build();

        return httpLocation;
	}

	private int getVersionNumber() {
		int version;
		try {
			String versionString = settings.getString("version", "1");
			version = Integer.parseInt(versionString); 
		} catch (Exception e) {
			version = 1;
		}
		return version;
	}

	private String getConfigString(String name) {
		return settings.getString(name, "");
	}

	private boolean getConfigBoolean(String name) {
		return settings.getBoolean(name, false);
	}
	
	private int getNumberOfHttpLocations() {
		 int count;
		 try {
			 count = Integer.parseInt(settings.getString("locationCount", "1"));
		 } catch (Exception e) {
			 count = 1;
		 }
		 
		 return (count > 0 ? count : 1);
    }

}