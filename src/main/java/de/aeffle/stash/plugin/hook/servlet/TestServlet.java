package de.aeffle.stash.plugin.hook.servlet;


import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import de.aeffle.stash.plugin.hook.http.agent.HttpAgent;
import de.aeffle.stash.plugin.hook.http.agent.Result;
import de.aeffle.stash.plugin.hook.http.location.HttpLocation;
import de.aeffle.stash.plugin.hook.persistence.AuditLogService;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;


@Component
public class TestServlet extends HttpServlet {
    private final AuditLogService auditLogService;
    private final RepositoryService repositoryService;
    private final SoyTemplateRenderer soyTemplateRenderer;

    @Inject
    public TestServlet(@ComponentImport SoyTemplateRenderer soyTemplateRenderer,
                       @ComponentImport RepositoryService repositoryService,
                       AuditLogService auditLogService) {
        this.auditLogService = checkNotNull(auditLogService);
        this.repositoryService = checkNotNull(repositoryService);
        this.soyTemplateRenderer = checkNotNull(soyTemplateRenderer);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Repository repository = getRepository(request);
        if (repository == null) {
            return;
        }

        Map<String, Object> data = new HashMap<>();
        data.put("repository", repository);
        String action = getAction(request);
        final String templateName;

        switch (action) {
            case "allAuditLog":
                templateName = "servlet.test.auditLog";
                data.put("auditLog", auditLogService.getAuditLog());
                break;

            case "auditLog":
                templateName = "servlet.test." + action;
                data.put("auditLog", auditLogService.getAuditLog(repository));
                break;

            case "createDummy":
                templateName = "servlet.test.auditLog";
                auditLogService.createAuditLogEntry(repository, "http://dummy.tld", 200, "dummy");
                data.put("auditLog", auditLogService.getAuditLog());
                break;

            case "test":
                templateName = "servlet.test." + action;
                data.put("error", "just a test");
                break;

            default:
                templateName = "servlet.test." + action;
                String url = request.getParameter("url");
                String method = request.getParameter("method");
                if (method == null) {
                    method = "GET";
                }
                boolean skipSslValidation = true;


                if (url != null) {
                    data.put("url", url);
                    data.put("method", method);
                    Result result = doHttpRequest(url, method, skipSslValidation);
                    data.put("status", result.getStatusCode());
                    data.put("message", result.getMessage());
                }
                break;
        }

        render(response, templateName, data);
    }


    private Result doHttpRequest(String url, String method, boolean skipSslValidation) {
        HttpLocation.HttpLocationBuilder builder = HttpLocation.builder()
                .url(url)
                .httpMethod(method)
                .postContentType("application/json")
                .postData("");

        if (skipSslValidation) {
            builder.sslValidationDisabled(true);
        }

        HttpLocation location =  builder.build();


        Result result = null;
        try {
            HttpAgent httpAgent = new HttpAgent(location);
            result = httpAgent.doRequest();
        } catch (Exception e) {
            result = Result.builder()
                .message("Exception was thrown: " + e.getClass() + " - " + e.getMessage())
                .build();
        }
        return result;
    }


    private void render(HttpServletResponse resp,
                        String templateName,
                        Map<String, Object> data)
            throws IOException, ServletException
    {
        resp.setContentType("text/html;charset=UTF-8");
        try {
            soyTemplateRenderer.render(resp.getWriter(),
                    "de.aeffle.stash.plugin.stash-http-get-post-receive-hook:test-servlet-soy",
                    templateName,
                    data);
        } catch (SoyException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            }
            throw new ServletException(e);
        }
    }


    private Repository getRepository(HttpServletRequest request)
            throws IOException {
        // Get repoSlug from path
        String pathInfo = request.getPathInfo();
        String[] components = pathInfo.split("/");

        if (components.length < 5) {
            return null;
        }

        Repository repository = repositoryService.getBySlug(components[2], components[4]);
        if (repository == null) {
            return null;
        }
        return repository;
    }


    private String getAction(HttpServletRequest request)
            throws IOException {
        // Get action from path
        String pathInfo = request.getPathInfo();
        String[] components = pathInfo.split("/");

        if (components.length < 6) {
            return "default";
        }

        switch (components[5]) {
            case "auditLog":
            case "allAuditLog":
            case "createDummy":
            case "test":
                return components[5];
            default:
                return "default";
        }
    }

}
