/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ut.de.aeffle.stash.plugin.hook.testHelpers;

import com.atlassian.bitbucket.commit.Commit;
import com.atlassian.bitbucket.commit.CommitRequest;
import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.repository.Repository;

import java.util.Date;

import static org.mockito.Mockito.*;


public class CommitServiceMockBuilder {

	private CommitService commitService;

	public CommitServiceMockBuilder() {
		clear();
        mockAnyCommit();
	}

	public CommitServiceMockBuilder clear() {
		commitService = mock(CommitService.class);
		return this;
	}


	private CommitServiceMockBuilder mockAnyCommit() {
        Commit commit = mock(Commit.class);
        when(commit.getMessage()).thenReturn("hello-world");
        when(commit.getAuthorTimestamp()).thenReturn(new Date());
        when(commitService.getCommit(any(CommitRequest.class))).thenReturn(commit);
        return this;
    }

    public CommitService build() {
        return commitService;
    }
	
}
