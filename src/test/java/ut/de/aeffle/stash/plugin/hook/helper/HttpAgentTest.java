/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ut.de.aeffle.stash.plugin.hook.helper;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import de.aeffle.stash.plugin.hook.http.location.HttpLocation;
import org.junit.Rule;
import org.junit.Test;

import de.aeffle.stash.plugin.hook.http.agent.HttpAgent;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;


public class HttpAgentTest {
    @Rule
    public WireMockRule wireMockRule = new WireMockRule(8888);

    @Test
    public void getRequestNoAuthTest()
            throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, IOException {
        //GIVEN
        stubFor(get(urlEqualTo("/api/call"))
                .willReturn(aResponse().withStatus(200)));

        //WHEN
        HttpLocation httpLocationTranslated = HttpLocation.builder()
                .url("http://localhost:8888/api/call")
                .build();

        HttpAgent httpAgent = new HttpAgent(httpLocationTranslated);
        httpAgent.doRequest();

        //THEN
        verify(getRequestedFor(urlEqualTo("/api/call")));
    }

    @Test
    public void getRequestDetailedNoAuthTest()
            throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, IOException {
        //GIVEN
        stubFor(get(urlEqualTo("/api/call"))
                .willReturn(aResponse().withStatus(200)));

        //WHEN
        HttpLocation httpLocationTranslated = HttpLocation.builder()
                .url("http://localhost:8888/api/call")
                .authEnabled(false)
                .httpMethod("GET")
                .build();

        HttpAgent httpAgent = new HttpAgent(httpLocationTranslated);
        httpAgent.doRequest();

        //THEN
        verify(getRequestedFor(urlEqualTo("/api/call")));
    }

    @Test
    public void getRequestWithRedirectTest()
            throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, IOException {
        //GIVEN
        stubFor(get(urlEqualTo("/api/old-call"))
                .willReturn(temporaryRedirect("/api/new-call")));
        stubFor(get(urlEqualTo("/api/new-call"))
                .willReturn(aResponse().withStatus(200)));

        //WHEN
        HttpLocation httpLocationTranslated = HttpLocation.builder()
                .url("http://localhost:8888/api/old-call")
                .build();

        HttpAgent httpAgent = new HttpAgent(httpLocationTranslated);
        httpAgent.doRequest();

        //THEN
        verify(getRequestedFor(urlEqualTo("/api/old-call")));
        verify(getRequestedFor(urlEqualTo("/api/new-call")));
    }


    @Test
    public void getRequestWithBasicAuth()
            throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, IOException {
        //GIVEN
        stubFor(get(urlEqualTo("/api/call"))
                .withBasicAuth("john.doe", "secret")
                .willReturn(aResponse().withStatus(200)));

        //WHEN
        HttpLocation httpLocationTranslated = HttpLocation.builder()
                .url("http://localhost:8888/api/call")
                .authEnabled(true)
                .user("john.doe")
                .pass("secret")
                .build();

        HttpAgent httpAgent = new HttpAgent(httpLocationTranslated);
        httpAgent.doRequest();

        //THEN
        verify(getRequestedFor(urlEqualTo("/api/call"))
                .withHeader("Authorization", equalTo("Basic am9obi5kb2U6c2VjcmV0")));
    }

    @Test
    public void getRequestWithError500()
            throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, IOException {
        //GIVEN
        stubFor(get(urlEqualTo("/api/call"))
                .willReturn(aResponse().withStatus(500)));

        //WHEN
        HttpLocation httpLocationTranslated = HttpLocation.builder()
                .url("http://localhost:8888/api/call")
                .build();

        HttpAgent httpAgent = new HttpAgent(httpLocationTranslated);
        httpAgent.doRequest();

        //THEN
        verify(getRequestedFor(urlEqualTo("/api/call")));
    }


}
