/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ut.de.aeffle.stash.plugin.hook.helper;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import de.aeffle.stash.plugin.hook.http.agent.HttpAgent;
import de.aeffle.stash.plugin.hook.http.location.HttpLocation;
import org.apache.http.client.fluent.Request;
import org.junit.Rule;
import org.junit.Test;

import javax.net.ssl.SSLHandshakeException;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.ThrowableAssert.catchThrowable;


public class HttpAgentSSLTest {
    @Rule
    public WireMockRule wireMockRule = new WireMockRule(wireMockConfig().httpsPort(8443));

    @Test
    public void getRequestNoAuthTest()
            throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, IOException {
        //GIVEN
        stubFor(get(urlEqualTo("/api/call"))
                .willReturn(aResponse().withStatus(200)));

        //WHEN
        HttpLocation httpLocationTranslated = HttpLocation.builder()
                .url("https://localhost:8443/api/call")
                .sslValidationDisabled(true)
                .build();

        HttpAgent httpAgent = new HttpAgent(httpLocationTranslated);
        httpAgent.doRequest();

        //THEN
        verify(getRequestedFor(urlEqualTo("/api/call")));
    }


    @Test
    public void httpsGetThrowsAnErrorWhenNotAcceptingCertificates() {
        //GIVEN
        stubFor(get(urlEqualTo("/my"))
                .willReturn(aResponse()
                        .withStatus(200)));

        //WHEN
        Throwable thrown = catchThrowable(() -> {
            HttpLocation httpLocationTranslated = HttpLocation.builder()
                    .url("https://localhost:8443/api/call")
                    .build();

            HttpAgent httpAgent = new HttpAgent(httpLocationTranslated);
            httpAgent.doRequest();
        });

        //THEN
        assertThat(thrown)
                .isInstanceOf(SSLHandshakeException.class)
                .hasMessageContaining("unable to find valid certification path to requested target");
    }
}
