/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ut.de.aeffle.stash.plugin.hook.testHelpers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.atlassian.bitbucket.auth.AuthenticationContext;
import com.atlassian.bitbucket.user.ApplicationUser;


public class AuthenticationContextMockFactory {

	private AuthenticationContext authenticationContext;
	private ApplicationUser applicationUser;

	public AuthenticationContextMockFactory() {
		clear();
	}

	public AuthenticationContextMockFactory clear() {
		createNewMocks();
		loadDefaults();
		return this;
	}

	private void createNewMocks() {
		authenticationContext = mock(AuthenticationContext.class);
		applicationUser = mock(ApplicationUser.class);
		
		when(authenticationContext.getCurrentUser()).thenReturn(applicationUser);
	}

	private void loadDefaults() {
		when(applicationUser.getDisplayName()).thenReturn("");
		when(applicationUser.getEmailAddress()).thenReturn("");
		when(applicationUser.getName()).thenReturn("");
        when(applicationUser.getSlug()).thenReturn("");
	}

	public void setDisplayName(String name) {
		when(applicationUser.getDisplayName()).thenReturn(name);
	}
	
	public void setEmailAddress(String email) {
		when(applicationUser.getEmailAddress()).thenReturn(email);
	}
	
	public void setName(String name) {
		when(applicationUser.getName()).thenReturn(name);
	}

    public void setSlug(String slug) {
        when(applicationUser.getSlug()).thenReturn(slug);
    }

	public AuthenticationContext getContext() {
		return authenticationContext;
	}
	
}
